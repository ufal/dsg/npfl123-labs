# NPFL123 Labs Homework 10, deadline: May 10th

This time, your task will be to train and evaluate an ASR system on a small dataset.
We will be using [Kaldi](https://kaldi-asr.org/) for these instructions since this is an advanced neural toolkit fairly similar in setup to what IBM is using (i.e. separate acoustic & language models).

If you like, you can try out the same thing out with the end-to-end neural [ESPNet](https://github.com/espnet/espnet) toolkit instead, but you'd probably need a GPU to train it fast enough.

## What you should do

1. [Download and install Kaldi](https://kaldi-asr.org/doc/install.html). 

    * Clone the Kaldi git repo
    * Go to `tools/` and follow the [INSTALL](https://github.com/kaldi-asr/kaldi/blob/master/tools/INSTALL) guide to install all required tools.
    * Go to `src/` and follow [INSTALL](https://github.com/kaldi-asr/kaldi/blob/master/src/INSTALL) again. If you don't (want to) have [Intel MKL](https://kaldi-asr.org/doc/matrixwrap.html) installed, you can use `./configure --mathlib=ATLAS` to use ATLAS as the math backend (just install the `libatlas-base-dev` package on Ubuntu for that).

2. Install the [KenLM](https://github.com/kpu/kenlm) language modelling toolkit. Clone the repo to anywhere you like, e.g., inside the `tools/` Kaldi directory, then follow the [build instructions](https://github.com/kpu/kenlm/blob/master/BUILDING). Note that you need Boost and some other libraries installed, as shown on the last line of the build instructions.

3. [Get the Easy-Kaldi repository](https://github.com/JRMeyer/easy-kaldi.git) -- this code will make training Kaldi *much* easier for us. Just clone it into the `egs/` directory.

4. We will be working with the [Free Spoken Digits Dataset](https://github.com/Jakobovski/free-spoken-digit-dataset/). Just clone the repo inside `egs/easy-kaldi/easy-kaldi`.
   * Since Easy-Kaldi is set-up to work with 16 kHz audio data and this dataset is recorded with 8 kHz sampling, you'll need to set a few configuration options. Change `--sample-frequency=16000` to `--sample-frequency=8000` in the following files: `config/mfcc.conf`, `config/mfcc_hires.conf`, `config/pitch.conf`, `config/plp.conf`.

5. Now create an `input_fsdd` directory and prepare all the needed input files for Easy-Kaldi (see the README):
   * `lexicon.txt` and `lexicon_nosil.txt` are basically pronunciation dictionaries for all words. Since your words are only English digits 0-9, you need to know the pronunciation for them. Look them up in the [CMU Pronouncing Dictionary](http://www.speech.cs.cmu.edu/cgi-bin/cmudict) and put them into the lexicon file, one per line. In addition to that, add `<unk>` (unknown phoneme) and  `!SIL` (silence). Their "pronunciation" can be anything that doesn't match the other phonemes. The final `lexicon.txt` file should look something like this:
        ```
        !SIL sil
        <unk> spn
        one W AH N
        two T UW
        [...]
        ```
        The `lexicon_nosil.txt` file is exactly the same -- just remove the `!SIL` line. Note that you can't have empty lines in either of these files.
   
   * `task.arpabo` is your language model. Usually, you would need a text file with "all transcriptions" to build it, so in this case, a text file with one digit per line ([like this](digits.txt)) will be fine. We'll use KenLM to build the language model: 
        ```
        ../../../../tools/kenlm/build/bin/lmplz -o 3 --discount_fallback < digits.txt > task.arpabo
        ```
        Adjust the path to KenLM if you put it somewhere else than `tools/` or if you're running this from somewhere else than the `input_fsdd` directory itself. Notice that we're building a trigram model even though our "sentences" always have one word -- have a look inside the `task.arpabo` file to see why :-).

   * Now you need to split your training and test data. Go to your `free-spoken-digit-dataset` checkout and create two subdirectories, `train_recordings` and `test_recordings`. Since all the recordings from the dataset are in the same directory, it's up to use to split it up. We'll split it by speaker:
        ```
        cd train_recordings; ln -s ../recordings/*{theo,yweweler,jackson}* .; cd ..
        cd test_recordings; ln -s ../recordings/*nicolas* .; cd ..
        ```
        Check that you have 1500 training and 500 test files.

   * The files `train_audio_path` and `test_audio_path` in your `input_fsdd` directory should contain just one line each -- absolute paths to `free-spoken-digit-dataset/train_recordings` and `free-spoken-digit-dataset/test_recordings`, respectively.

   * Now you need to create the "transcript" files. Since the "transcript" is basically contained in the name of each file, just using `ls` and some `sed` trickery will do the job:
        ```
        ls ../free-spoken-digit-dataset/train_recordings > transcripts.train
        sed -i 's/\.wav/ /;/^0/s/$/zero/;/^1/s/$/one/;/^2/s/$/two/;/^3/s/$/three/;/^4/s/$/four/;/^5/s/$/five/;/^6/s/$/six/;/^7/s/$/seven/;/^8/s/$/eight/;/^9/s/$/nine/;' transcripts.train
        ```
        The `-i` switch changes the file in place. We basically just remove the `.wav` suffix, then add the right digits based on the filename. 
        
        Do the same for the `transcripts.test` file.

6. Now that your files are done, you need to first align the data on the phoneme level. This is actually done by training a GMM-HMM ASR model in Kaldi. Run this:
   ```
   ./run_gmm.sh fsdd 001
   ```
   The `fsdd` corresponds to the name of your `input_fsdd` directory, the `001` parameter can be anything (indicates experiment number). After you run this, you can read the GMM-HMM model's WER in `WER_triphones_fsdd_001.txt`.

7. Now you can finally train and evaluate the neural ASR model -- it's using the HMM phoneme alignments. Run this:
   ```
   ./run_nnet3.sh fsdd 300 2
   ```
   You can play around with the parameters (300 is the network hidden state dimension, 2 is the number of epochs). Here we just show some values that seemed to give reasonable performance for this data. After you run it, you can check the WER in `WER_nnet3_easy.txt`. 
   
   Have a look at the recognized outputs for each file, they're hidden in `exp_fsdd/nnet3/easy/decode/log/decode.*.log` -- look for lines starting with filenames, they contain the decoded text.

   Keep your file with WER since it'll get overwritten in the next step :-).

8. Now repeat the experiment, but with random files split instead of a split by speaker. Make a copy of the `input_fsdd` directory (`cp -r input_fsdd input_fsdd2`) and change stuff around:

    * You can keep the lexicons and the language model.

    * Create the new data split inside `free-spoken-digits-dataset` -- create directories `train_recordings2` and `test_recordings2` and run:
        ```
        cd train_recordings2; ls ../recordings | sort -R --random-source=../recordings/0_yweweler_0.wav | head -n 1500 | xargs '-I{}' ln -s ../recordings/'{}' .; cd ..
        cd test_recordings2; ls ../recordings | sort -R --random-source=../recordings/0_yweweler_0.wav | tail -n 500 | xargs '-I{}' ln -s ../recordings/'{}' .; cd ..
        ```
        Notice that we've shuffled the data randomly using `sort -R`, but we keep the same random seed (using one of the WAV files), so the resulting order is always the same. We then take the first 1500 and the last 500 files and link to them from the new directories.

    * Update the `train_audio_path`, `test_audio_path`, `transcripts.train` and `transcripts.test` files to use your new data split (it's easier to recreate the transcripts using the code above).

    * Now rerun the both steps of training and evaluation.


## What you should submit

Submit a single Markdown file into your subdirectory of this directory, here in this repository. Include this:

* Your WER for four settings:
    * Per-speaker split, GMM-HMM
    * Per-speaker split, neural
    * Random split, GMM-HMM
    * Random split, neural

* A short report -- please try to explain:
    * why the WER results ended up the way they did,
    * which digits are most difficult to recognize for the per-speaker neural model and why.


## Late submission penalty

Run an additional experiment where you concatenate both the WAV files and transcriptions to create “sentences” of up to 4 words. 
Use the `sox` utility to concatenate the WAV files (`sox 1.wav 2.wav 3.wav output.wav`).
Only use the data _split by speaker_ for this additional experiment. 
Concatenate only instances in the training set with each other, do the same later for the test set. 
Join the data to sentences fo 2, 3, 4 words, and create 1500 2-word sentences, 1500 3-word sentences, and 1500 4-word sentences.
Create two variants of the concatenated training + test sets:
* Random concatenation (select data for joining completely randomly, but always use samples _from the same speaker_).
* Even/odd concatenation (only join together odd and even digits from the same speaker, e.g. 1-3-7, 2-8, 3-9, 4-4-2-0 is fine, but 4-3 won't occur).

Report the same metrics as for the basic assignment.


## Further reading

If you want the real thing, not the Easy-Kaldi abstraction:

* [Some conceptual background](https://towardsdatascience.com/how-to-start-with-kaldi-and-speech-recognition-a9b7670ffff6)
* [Slightly outdated, but detailed Kaldi training tutorial](http://jrmeyer.github.io/asr/2016/12/15/DNN-AM-Kaldi.html)
* [The actual Kaldi documentation](http://kaldi-asr.org/doc/tutorial.html)
