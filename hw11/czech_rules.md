Basic Czech grapheme-to-phoneme conversion rules
================================================

This is actually a very rough simplification, but it'll do for our example and it should (hopefully) produce intelligible Czech speech. Phonemes here are shown in the [SAMPA notation for Czech used by MBROLA](https://github.com/numediart/MBROLA-voices/tree/master/data/cz2). Let us know if you find a mistake.

* _ch_ (two graphemes) are actually pronounced as one phoneme, [x]

* Similarly, _dz_, _dž_ are pronounced as single phonemes [dz], [dZ]

* _di_, _dí_, _ti_, _tí_, _ni_, _ní_ are pronounced as _ďi_, _ďí_, _ťi_, _ťí_, _ňi_, _ňí_

* All _y_'s are pronounced the same as _i_'s (except for the previous rule), _ů_ is the same as _ú_

* _ě_ is a tricky letter:
    * _dě_, _tě_, _ně_ is pronounced as _ďe_, _ťe_, _ňe_
    * _bě_, _pě_, _vě_ is pronounced as _bje_, _pje_, _vje_
    * _mě_ is pronounced as _mňe_

* The vowel groups _ie_, _ia_, _iu_, _ijo_, _ii_, _ií_ are pronounced as _ije_, _ija_, _iju_, _ijo_, _iji_, _ijí_.

* At the end of each word, all voiced consonants turn to unvoiced: _b_ → _p_, _v_ → _f_, _d_ → _t_, _z_ → _s_, _dz_ → _c_, _ž_ → _š_, _dž_ → _č_, _ď_ → _ť_, _g_ → _k_, _h_ → _ch_

* Czech uses a [regressive assimilation](https://en.wikipedia.org/wiki/Assimilation_(phonology)) of consonants ([see here for Czech info](https://prirucka.ujc.cas.cz/?id=908)). If multiple consonants occur together, the last one of the group determines whether all consonants in the group are voiced. The voiced-unvoiced pairs are the same as in the rule above; here are some examples:
    * _sbírat_ is pronounced _zbírat_ -- because _b_ is voiced, _s_ becomes _z_
    * _leckdo_ is pronounced _ledzgdo_ -- because _d_ is voiced, _k_ becomes _g_ and _c_ becomes _dz_
    * _tužka_ is pronounced _tuška_ -- because _k_ is unvoiced, _ž_ becomes _š_
    * _vztah_ is pronounced _fstach_ -- because _t_ is unvoiced, _z_ becomes _s_ and _v_ becomes _f_ (also _h_ turns to _ch_ at the end of the word, see above)

* Stress goes on the 1st syllable in each word. There are a few exceptions:

    * 1-syllable prepositions (_bez_, _do_, _na_, _nad_, _o_, _od_, _po_, _pod_, _pro_, _před_, _při_, _u_, _za_) take the stress of the following word.

    * Some short words have no stress (_ho_, _je_, _jsem_, _jsi_, _jsme_, _jsou_, _jste_, _li_, _mě_, _mi_, _mně_, _mu_, _se_, _si_, _tě_, _ti_, _to_, _tu_)
