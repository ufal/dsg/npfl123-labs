# NPFL123 Labs Homework 11, deadline: May 17th

This time, your task will be to create a grapheme-to-phoneme conversion that works with the [MBROLA](https://github.com/numediart/MBROLA) concatenative speech synthesis system. By default, we'll assume you'll use Czech or English with this homework. Since different languages have very different grapheme-to-phoneme ratios, talk to us if you want to try it out for any other language instead.

## What you should do

1. Install [MBROLA](https://github.com/numediart/MBROLA). On Debian-based Linuxes, this should be as simple as `sudo apt install mbrola`. Building for any other Linux shouldn't be too hard either. On Windows, you can use [WSL](https://en.wikipedia.org/wiki/Windows_Subsystem_for_Linux), or you can get [native Windows binaries here](https://github.com/thiekus/MBROLA/releases/tag/3.3).

2. Install a MBROLA voice for your language. On Debian-derivatives (incl. WSL), you can go with `sudo apt install mbrola-<voice>` and your voices will into `/usr/share/mbrola`, otherwise you just need to download the voice somewhere. 

    * For Czech, [cz2](https://github.com/numediart/MBROLA-voices/tree/master/data/cz2) is a good voice, cz1 is lacking some rather common diphone combinations.

    * For English, you can go with [en1](https://github.com/numediart/MBROLA-voices/tree/master/data/en1).

    You can try out that it's working by running MBROLA through one of the test files included with the voice. There's always a “test” subdirectory with some “.pho” files.
    ```
    mbrola -e /path/to/cz2  path/to/test/some_file.pho output.wav
    ```

3. Implement a simple normalization script. It should be able to expand numbers (just single digits) and abbreviations from a list.

    * Ignore the fact that you sometimes need context for the abbreviations.
    * Add the following abbreviations to your list to test it: _Dr_, _Prof_, _kg_, _km_, _etc_/_atd_.

4. Add a grapheme-to-phoneme conversion to your script that produces a phoneme sequence like this: 

   Czech:
   ```
   a    100
   h\   50
   o    70
   j    50
   _    200
   ```

   English:
   ```
   h    50
   @    70
   l    50
   @U   200
   _    200
   ```
   It's basically a two-column tab/space-separated file. The first column is a phoneme, the 2nd column denotes the duration in milliseconds.

   The available phonemes for each language are defined in the voices' README files ([cs](https://github.com/numediart/MBROLA-voices/tree/master/data/cz2), [en](https://github.com/numediart/MBROLA-voices/tree/master/data/en1)). MBROLA uses the SAMPA phonetic notation. The `_` denotes a pause in any language.

   Use the following simple rules for phoneme duration:
    * Consonant – 50 ms
    * Short vowel (any vowel without “:” in Czech SAMPA, any 1-character vowel in English SAMPA): stressed – 100 ms, unstressed – 70 ms
    * Long vowel (vowels with “:” in Czech SAMPA, 2-character vowels in English SAMPA): stressed – 200 ms, unstressed – 150 ms

   If you inspect the MBROLA test files or the [description here](https://www.phon.ucl.ac.uk/courses/spsci/spc/lab8.html), you'll see that there's an optional third column for voice melody, saying which way F0 should develop during each phoneme. For our exercise, we'll ignore it. It'll give you a rather robotic, but understandable voice. What you should do, though is:

   * Add a 200 ms pause after each comma or dash.
   * Add a 500 ms pause after sentence-final punctuation (full stop, exclamation or question mark).
   
   Finally, the actual grapheme-to-phoneme rules are very different for both languages.

    * For Czech, you can do almost everything by starting from ortography and applying [some relatively simple rules](czech_rules.md). 
        * You should also add a dictionary for exceptions – add 10 random foreign words to it with their correct SAMPA pronunciations, to test that it works correctly.

    * For English, you can't do without a dictionary. Use the [CMU Pronouncing Dictionary](http://www.speech.cs.cmu.edu/cgi-bin/cmudict) which you can download as a whole. 
        * Since the dictionary uses [Arpabet](https://en.wikipedia.org/wiki/ARPABET) and you want SAMPA for MBROLA, you'll need to create an [Arpabet-to-SAMPA mapping](arpabet_to_sampa.md) to use it. 
        * The dictionary has stress marks (“1”, “2”, “3” etc. after vowels, so you can treat vowels with “1” or “2” as stressed, the rest as unstressed).
        * Let the system spell out any word that it doesn't find in the dictionary (get the pronunciation of each letter).

4. Take a random Wikipedia article (say, “article of the day”) in your target language, produce the g2p conversion for the first paragraph, then run MBROLA on it and save a WAV file.
  

## What you should submit

Submit all of this into a subdirectory with your name in this directory:

* Your normalization and grapheme-to-phoneme script that will take plain text input and outputs a MBROLA instructions file.
* The text of the paragraph on which you tried your conversion system.
* Your script's output on the paragraph (the “.pho” file for MBROLA).
* Your resulting MBROLA-produced WAV file.


## Late submission penalty

Here the penalty is less of a choice -- implement the **Czech version** of the task, which is probably slightly harder than English.