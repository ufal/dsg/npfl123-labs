English Arpabet to SAMPA mapping
================================

From [CMU Pronouncing Dictionary](http://www.speech.cs.cmu.edu/cgi-bin/cmudict) to [MBROLA en1 voice SAMPA](https://github.com/numediart/MBROLA-voices/tree/master/data/en1) phonemes. Might not be completely accurate, let us know if you find any problems.

Vowels:

| CMU/Arpabet | SAMPA | IPA | example |
| --- | ---   | --- | --- |
| AA  | A:  | ɑ   | balm, bot |
| AE  | {   | æ   | bat |
| AH  | V   | ʌ   | butt |
| AO  | Q   | ɔ   | story |
| AW  | aU  | aʊ  | bout |
| AY  | aI  | aɪ  | bite |
| EH  | e   | ɛ   | bet |
| ER  | 3:  | ɝ   | bird |
| EY  | eI  | eɪ  | bait |
| IH  | I   | ɪ   | bit |
| IY  | i:  | i   | beat |
| OW  | `@U`  | oʊ  | boat |
| OY  | OI  | ɔɪ  | boy |
| UH  | U   | ʊ   | book |
| UW  | u:  | u   | boot |

Consonants:

| CMU/Arpabet | SAMPA | IPA | example |
| --- | ---   | --- | --- |
| B   | b   | b   | buy |
| CH  | tS  | tʃ  | China |
| D   | d   | d   | die |
| DH  | D   | ð   | thy |
| F   | f   | f   | fight |
| G   | g   | ɡ   | guy |
| HH  | h   | h   | high |
| JH  | dZ  | dʒ  | jive |
| K   | k   | k   | kite |
| L   | l   | l   | lie |
| M   | m   | m   | my |
| N   | n   | n   | nigh |
| NG  | N   | ŋ   | sing |
| P   | p   | p   | pie |
| R   | r   | ɹ   | rye |
| S   | s   | s   | sigh |
| SH  | S   | ʃ   | shy |
| T   | t   | t   | tie |
| TH  | T   | θ   | thigh |
| V   | v   | v   | vie |
| W   | w   | w   | wise |
| Y   | j   | j   | yacht |
| Z   | z   | z   | zoo |
| ZH  | Z   | ʒ   | pleasure   |


