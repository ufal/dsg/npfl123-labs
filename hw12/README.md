# NPFL123 Labs Homework 11, deadline: May 24th

This time, you will implement a basic information-retrieval-based chatbot. We'll just use TF-IDF for retrieval, with no smart reranking.

## What you should do

1. Get the [DailyDialogue dataset](http://yanran.li/dailydialog). This is a dataset of basic day-to-day dialogues, so it's great for chatbots.
    Have a look at the data format, it's not very hard to parse.

2. Implement an IR chatbot module:
    
    * Load the DialyDialogue data into memory so that you know which turn follows which. Use `train.zip/dialogues_train.txt` for this.

    * From your data, create a _Keys_ dataset, containing all turns in all dialogues except the last one. Then create a _Values_ dataset,
      which always contain the immediately next turn for each dialogue. 

        * Say there's just 1 dialogue with 5 turns (represented just by numbers here). Keys should contain `[0, 1, 2, 3]` 
            and the corresponding Values are `[1, 2, 3, 4]`.

    * Use [TfidfVectorizer](https://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.TfidfVectorizer.html) from Scikit-Learn
      as the main “engine”. 
        
        * Create a vectorizer object and call `fit_transform` on the Keys set to train your matching TF-IDF matrix (store this matrix for later). Feel free to play around 
            with this method's parameters, especially with the `ngram_range` -- setting it slightly higher than the default `(1,1)` might give you
            better results.

    * For any input sentence, what your chatbot should do is:

        * Call `transform` on your vectorizer object to obtain TF-IDF scores.

        * Get the [cosine similarity](https://scikit-learn.org/stable/modules/generated/sklearn.metrics.pairwise.cosine_similarity.html) of the sentence's
            TF-IDF to all the items in the Keys dataset.

        * Find the top 10 Keys' indexes using [`numpy.argpartition`](https://numpy.org/doc/1.18/reference/generated/numpy.argpartition.html) 
            (see the [example](https://stackoverflow.com/a/23734295/1799871) here).
            Now get the corresponding top 10 Values (at the same indexes). Choose one of them at random and use it as output.

            * Instead of choosing at random, you could do some smart reranking, but we'll skip that in this exercise.

3. Integrate your chatbot into DialMonkey. Create a module inside `dialmonkey.policy` and add a corresponding YAML config file. You can call it `ir_<your_name>`.
    
    * While doing so, use the DailyDialog training data that are already stored in `data/dailydialog/dialogues_train.txt`. 
        Note that you need Git-LFS to download the file contents properly.

4. Take the 1st sentence of the first 10 DailyDialogue validation dialogues (`validation.zip/dialogues_validation.txt`) and see what your chatbot tells you.

## What you should commit

* Commit your chatbot module and your configuration file into the DialMonkey repository.
* Commit the first 10 DailyDialogue validation opening lines along with your chatbot's responses in a text file into this repository.

## Late submission penalty

The additional task for the late submission will be actually implementing the reranking step.
Select a classifier of your choosing (it can be logistic regression, a neural net, whatever you like) and implement a binary classifier
based on word and/or n-gram features (if you choose an RNN, words are enough, but if you go with logistic regression or SVM, please use also bigrams at least).
This classifier should take the context and a response on the input and give a 0/1 decision if the response belongs to this context.
Take the positive instances directly from your data (the real context-response pairs), and you can sample the same number of negative instances randomly 
(by selecting a different response, not equal to the real one).

Use this trained classifier to select among the top 10 responses (the highest confidence in the positive class wins). 
Commit the chatbot's outputs both with and without this classifier/ranker.

## Further reading

More low-level stuff on TF-IDF:
* https://towardsdatascience.com/tf-idf-for-document-ranking-from-scratch-in-python-on-real-world-dataset-796d339a4089
* https://towardsdatascience.com/natural-language-processing-feature-engineering-using-tf-idf-e8b9d00e7e76
* https://programminghistorian.org/en/lessons/analyzing-documents-with-tfidf
