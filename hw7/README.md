# NPFL123 Labs Homework 7, deadline: April 19th

This week is basically a continuation of the last one -- filling in the blank API call placeholders 
you created last time. 
If you want to, you can complete the 6th and 7th homework at the same time (just let us know).

## What you should do

1. Implement all API/backend queries you need for the policy you implemented in [the last homework](../hw6/).
   The implementation can be directly inside `dialmonkey.policy.<your_domain>`, or you can create
   a sub-package (a subdirectory, where you put the main policy inside `__init__.py` and any auxiliary
   stuff into other files in the same directory).

2. Test your policy with outputs on at least 3 of the test dialogues you created in 
   the [1st homework](../hw1). You can of course make slight alterations if your policy doesn't behave
   exactly as you imagined the first time.

   Also, don't worry that the output is just dialogue acts at the moment.


## What you should submit

* Commit your updated policy with API calls into the Dialmonkey repository.
* Commit logs of your test dialogues here into this repository.
   
