# NPFL123 Labs Homework 9, deadline: May 3rd

In this homework, you will integrate the chatbot for your domain into an online assistant of your choice.

## What you should do

1. Choose a service that you want to use for this homework. We prepared some instructions for 
   [Google Dialogflow](https://dialogflow.cloud.google.com/), [Alexa Skills](https://developer.amazon.com/alexa/console/ask/) 
   and [Facebook Messenger](https://github.com/vernwalrahul/my-messenger-bot/blob/master/app.py), 
   but you're free to use [IBM Watson Assistant](https://www.ibm.com/cloud/watson-assistant/)
   as shown in the 5th lecture, or any other platform of your liking.

   Note that Dialogflow and Alexa are unfortunately not available for Czech.

2. Implement the frontend on your selected platform. You can either carry over intents, slots & values
   from your NLU directly into Dialogflow/Alexa/Watson, or you can work with free text in Messenger
   and run the NLU in the backend. Dialogflow and Alexa also allow a workaround if you want to run your NLU
   in the backend:
   * For Alexa, it's a bit tricky -- the only way to get free text is to use the [SearchQuery](https://developer.amazon.com/blogs/alexa/post/a2716002-0f50-4587-b038-31ce631c0c07/enhance-speech-recognition-of-your-alexa-skills-with-phrase-slots-and-amazon-searchquery) built-in slot.
     You can set up an intent where the only part of the utterance is this slot.

   * For Dialogflow, you can make use of the [Default fallback intent](https://cloud.google.com/dialogflow/docs/intents-default)
     which gets triggered whenever no other intent is recognized.

3. Implement a backend that will connect to your frontend – handle its calls and route them to your dialogue system
   in Dialmonkey (either with NLU already processed, or with free text as input).

   You can use the `get_response` method in [`dialmonkey.conversation_handler.ConversationHandler`](https://gitlab.com/ufal/dsg/dialmonkey/-/blob/master/dialmonkey/conversation_handler.py) to control the system outside of the console.

   * For Alexa, you can use the [Flask-Ask](https://flask-ask.readthedocs.io/en/latest/) package as a base. You 
     can have a look at [an Alexa Skill Ondrej made](https://github.com/tuetschek/switch_radio) for inspiration
     (not many docs, though, sorry).
   
   * For Dialogflow, you can use [Flask-Assistant](https://flask-assistant.readthedocs.io/en/latest/index.html).
     If you want to get free text of the requests, have a look at [this snippet](https://stackoverflow.com/questions/55025211/get-user-message-on-flask-assistant).

   * For Messenger, you can use [Flask](https://flask.palletsprojects.com/en/1.1.x/) or other webserver of your choice. You need to implement GET method handler to verify the token and POST method handler to receive message and send the reply.
     Take a look to at [example implementation](https://github.com/vernwalrahul/my-messenger-bot/blob/master/app.py) (feel free to reuse it).  
     You can also use [pymessenger](https://github.com/davidchua/pymessenger).


4. Link your frontend to your backend. You can use [ngrok](https://ngrok.com/) to do so for testing purposes, if you can't keep it running
   on a public webserver.
   * In Alexa, set your backend address under “Endpoint” (left menu).
   * In Dialogflow, set your backend address under “Webhooks” for the individual intents (under the “Fulfillment” menu of each intent).
   * In Messenger, visit the [dev page](https://developers.facebook.com/). Create an account, add a Messenger app and create a sample page. 
     Then link the page to your app and obtain a token for use in your webserver. Add a callback URL pointing to your webserver and the 
     verification token. See the [tutorial here](https://developers.facebook.com/docs/messenger-platform/getting-started/quick-start) for details.

## What you should submit

* Commit your frontend export into this repository:
  * In Alexa, go to “JSON Editor” in the left menu and copy out the contents into a file.
  * In Dialogflow, go to your agent settings (cogged wheel next your agent/skill/app name on the top left), then select the “Export & Import” tab
    and choose “Export as ZIP”. Please commit the resulting subdirectory structure, not the ZIP file.
  * Nothing is required for Messenger at this point.
   
* Commit your backend code into this repository. This code will probably import a lot from Dialmonkey and require it to be installed -- that's expected.
