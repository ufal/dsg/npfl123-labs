# NPFL123 Labs Homework 8, deadline: April 26th

In this homework, you will complete the chatbot for your domain by creating a template-based NLG component.

## What you should do

1. Implement a template-based NLG with the following features:
    * The system is (mostly) generic, templates for your domain are listed in a YAML file, showing a DA -> template mapping.
    * The system is able to prioritize a mapping for a specific value -- e.g. `inform(price=cheap)` -> _“You'll save money here.”_ should get priority over `inform(price={price})` -> _“This place is {price}.”_
    * The system is able to put together partial templates (by concatenating), so you can get a result for e.g. `inform(price=cheap,rating=low)` even if you only have templates defined for `inform(price={price})` and `inform(rating={rating})`, not the specific slot combination. This doesn't need to search for best coverage, just take anything that fits, such as templates for single slots if you don't find the correct combination.
    * The system is able to produce multiple variations for certain outputs, e.g. `bye()` -> _Goodbye._ or _Thanks, bye!_

2. Create templates that cover your domain well.

3. Save your NLG system under `dialmonkey.nlg.<your-domain>` and add it into your `conf/<your-domain>.yaml` configuration file.

4. Test your NLG system with the test dialogues you used in the [previous homework](../hw7).


## What you should submit

* Commit your NLG system, the templates file, and the updated configuration file into the Dialmonkey repository.
* Commit logs of your test dialogues here into this repository.
   
