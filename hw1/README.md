# NPFL123 Labs Homework 1, deadline: March 21st

You will be building a dialogue system in (at least some of) the homeworks for this course. Your first task is to **choose a domain** and imagine how your system will look like and work like. Since you might later find that you don't like the domain, you are now required to **pick two**, so you have more/better ideas later and can choose only one of them for building the system.


## Requirements 

The required steps for this homework are:

1. Pick two domains of your liking that are suitable for building a dialogue system. Think of a reasonable backend (see below).
2. Write 5 example system-user dialogues for both domains, which are at least 5+5 turns long (5 sentences for both user and system). This will make sure that your domain is interesting enough. You do not necessarily have to use English here (but it's easier if we understand the language -- ask us if unsure).
3. Create a flowchart for your two domains, with labels such as “ask about phone number”, “reply with phone number”, “something else” etc. It should cover all of your example dialogues. Feel free to draw this by hand and take a photo, as long as it's legible.


## Inspiration

You may choose any domain you like, be it tourist information, information about culture events/traffic, news, scheduling/agenda, task completion etc. You can take inspiration from stuff presented in the first lecture, or you may choose your own topic.

Since your domain will likely need to be connected to some backend database, you might want to make use of some external public APIs -- feel free to choose under one of these links:
* [Top Ten Open Data APIs](https://www.programmableweb.com/news/10-top-open-data-apis/brief/2019/12/08), there are others on the same website
* [An informal listing of various Czech APIs](https://github.com/honzajavorek/cs-apis/)

You can of course choose anything else you like as your backend, e.g. portions of Wikidata/DBPedia or other world knowledge DBs, or even a handwritten “toy” database of a meaningful size, which you'll need to write to be able to test your system.
