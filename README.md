# NPFL123-labs

This repository contains the pages with lab homeworks for the [UFAL Dialogue Systems course](http://ufal.cz/npfl123), 2021 run.

The individual homework assignments will be listed on separate webpages below. The *deadline* for each homework is 12 days from the day it was assigned (i.e., Sunday next week), 23:59 CET. Additional homework will be assigned for missed deadlines.

To turn in your homework, create a pull request with a subdirectory with your name under the respective directory (e.g., [hw1](hw1/)). You can commit just a file containing a link to your own repo if you like/need to. The preferred format is Markdown for texts and Python for code, plus anything you need for other types of data.

### Assignments

* There will be no labs in the 1nd week of the semester
* [Lab 1: Mockup – Mar 9](hw1/README.md) -- deadline: Mar 21

