## Setting up your homework repository
The system of submitting the homeworks requires you to make a copy of the [dialmonkey](https://gitlab.com/ufal/dsg/dialmonkey) repository.
You will be submitting the homewokrs using git commits.

We provide an easy few-step recipe how to manage your repository.

### Preparing the repository
 1. Log into your MFF gitlab account, your username and password should be the same as in the CAS, see [this](https://www.ms.mff.cuni.cz/local/gitlab.html.cz).
 2. Import Dialmonkey as a new project. Choose **Private** visibility level.
```
New project -> Import project -> Repo by URL
```
![Import the project](assets/import.jpg)

 3. Invite us (*@duseo7af*, *@hudecekv*) to your project so we can see it.
```
Members -> Invite Member
```

 4. Clone the newly created repository.
 5. Change into the cloned directory and run
```
git remote show origin
```
You should see these two lines:
```
* remote origin
  Fetch URL: git@gitlab.mff.cuni.cz:your_username/dialmonkey.git
  Push  URL: git@gitlab.mff.cuni.cz:your_username/dialmonkey.git

```
 6. Add the original repository as your `upstream`:
```
git remote add upstream https://gitlab.com/ufal/dsg/dialmonkey
```
 7. You're all set!


### Submitting the homework assignment
 1. Make sure you're on your master branch
```
git checkout master
```
 2. Checkout new branch:
```
git checkout -b hw-0x-assigment-name
```
 3. Solve the assignment :)
 4. Add new files (if applicable) and commit your changes:
```
git add hw0x/solution.py
git commit -am 'commit message'
```
 5. Push to your *origin* remote repository:
```
git push origin hw-0x-assigment-name
```

 6. Create pull request in the web interface. Make sure you create the Merge request against your forked repository!
```
Merge requests -> New merge request
```
![Merge request interface](assets/mr.jpg)

 7. Send us link to the merge request.
 8. Enjoy your points!
 9. (Once approved, merge your changes in your master branch)

### Upgrading your repository
It might happen, that we'll make changes in the *upstream* repository.
To upgrade, do the following:
 1. Make sure you're on your master branch
```
git checkout master
```
 2. Fetch the changes
```
git fetch upstream master
```
 3. Apply the diff
```
git merge upstream master
```
